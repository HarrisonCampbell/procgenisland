﻿using System.Collections.Generic;

namespace Soup.Grids {

    /// <summary>
    /// Interface for all cell object
    /// </summary>
    public interface ICell {

        #region Variables

        /// <summary>
        /// The index of the cell
        /// </summary>
        int index { get; set; }

        /// <summary>
        /// The coloum of the cell
        /// </summary>
        int coloum { get; set; }

        /// <summary>
        /// The row of the cell
        /// </summary>
        int row { get; set; }

        /// <summary>
        /// The index's of the nearby cells
        /// </summary>
        List<int> nearbyCells { get; set; }

        #endregion

    }

    /// <summary>
    /// Interface for all grid items
    /// </summary>
    /// <typeparam name="T">Type of cell</typeparam>
    public interface IGrid<T> where T : ICell {

        #region Variables

        /// <summary>
        /// All of the cells that are stored in the grid
        /// </summary>
        Dictionary<int, T> cells { get; }
        
        #endregion

    }

}
