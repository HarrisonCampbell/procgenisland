﻿using System.Collections.Generic;

namespace Soup.Grids {

    /// <summary>
    /// Holds a hexagon cell
    /// </summary>
    public abstract class HexCell : ICell {

        #region Variables
        
        /// <summary>
        /// The index of the cell
        /// </summary>
        public int index { get; set; }
        
        /// <summary>
        /// The coloum of the cell
        /// </summary>
        public int coloum { get; set; }

        /// <summary>
        /// The row of the cell
        /// </summary>
        public int row { get; set; }

        /// <summary>
        /// A list of nearby nodes
        /// </summary>
        public List<int> nearbyCells { get; set; }

        /// <summary>
        /// The width of the grid the cell resides on
        /// </summary>
        public int gridWidth;

        /// <summary>
        /// The height of the grid the cell resides on
        /// </summary>
        public int gridHeight;

        #endregion

        #region Functions

        /// <summary>
        /// Get the corner points of a hexagon
        /// Organised in clockwise formation
        /// </summary>
        /// <returns>Returns the corner points of the hexagon</returns>
        public List<(int, int,int)> getCornerPoints() {

            List<(int, int, int)> cornerPoints = new List<(int, int, int)>();

            //For even rows
            if(row % 2 == 0) {

                if(coloum > 0 && row < gridHeight - 1) {
                    cornerPoints.Add((index - 1, index, index + gridWidth - 1)); // Top Left
                }
                if(coloum > 0 && row < gridHeight - 1) {
                    cornerPoints.Add((index, index + gridWidth - 1, index + gridWidth)); // Top
                }
                if (coloum < gridWidth - 1 && row < gridHeight - 1) {
                    cornerPoints.Add((index, index + 1, index + gridWidth)); // Top Right
                }
                if(coloum < gridWidth - 1 && row > 0) {
                    cornerPoints.Add((index - gridWidth, index, index + 1)); // Bottom Right
                }
                if(coloum > 0 && row > 0) {
                    cornerPoints.Add((index - gridWidth - 1, index - gridWidth, index)); // Bottom
                }
                if(coloum > 0 && row > 0) {
                    cornerPoints.Add((index - gridWidth - 1, index - 1, index)); // Bottom Left
                }

            } else {
                if (coloum > 0 && row < gridHeight - 1) {
                    cornerPoints.Add((index - 1, index, index + gridWidth)); // Top Left
                }
                if (row < gridHeight - 1) {
                    cornerPoints.Add((index, index + gridWidth, index + gridWidth + 1)); // Top
                }
                if (coloum < gridWidth - 1 && row < gridHeight - 1) {
                    cornerPoints.Add((index, index + 1, index + gridWidth + 1)); // Top Right
                }
                if (coloum < gridWidth - 1 && row > 0) {
                    cornerPoints.Add((index - gridWidth + 1, index, index + 1)); // Bottom Right
                }
                if (row > 0) {
                    cornerPoints.Add((index - gridWidth, index - gridWidth + 1, index)); // Bottom
                }
                if (coloum > 0 && row > 0) {
                    cornerPoints.Add((index - gridWidth, index - 1, index)); // Bottom Left
                }
            }
            

            return cornerPoints;
        }

        #endregion

        #region Constructors

        /// <summary>
        /// Construct a hex cell
        /// </summary>
        public HexCell() {
        }

        #endregion

    }

}
