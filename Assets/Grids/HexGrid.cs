﻿using System;
using System.Collections.Generic;

namespace Soup.Grids {
    
    /// <summary>
    /// Constructs a hexagon grid
    /// </summary>
    public class HexGrid<T> : IGrid<T> where T : HexCell {

        #region Variables

        /// <summary>
        /// Code that executes on each hexagon
        /// </summary>
        /// <param name="aHexagon">The selected hexagon we are executing on</param>
        public delegate void OnHexagon(T aHexagon);

        /// <summary>
        /// Code that executes on each hexagon line
        /// </summary>
        /// <param name="aPointOne">The first point on the line</param>
        /// <param name="aPointTwo">The second point on the line</param>
        public delegate void OnHexagonLine(HexCornerPoint aPointOne, HexCornerPoint aPointTwo);

        /// <summary>
        /// Code that executes on each hexagon point
        /// </summary>
        /// <param name="aPoint">The point on the hexagon</param>
        public delegate void OnHexagonPoint(HexCornerPoint aPoint);

        /// <summary>
        /// The width of the grid
        /// </summary>
        public int width { get; private set; }

        /// <summary>
        /// The height of the grid
        /// </summary>
        public int height { get; private set; }

        /// <summary>
        /// All of the cells in the grid categories by their index
        /// </summary>
        public Dictionary<int, T> cells { get; private set; }

        /// <summary>
        /// All of the significant corner points on the grid
        /// </summary>
        public Dictionary<(int, int, int), HexCornerPoint> cornerPoints;
        
        #endregion

        #region Functions

        /// <summary>
        /// Execute on each hexagon point
        /// </summary>
        /// <param name="aOnPoint">The code that will execute on each hexagon point</param>
        public void onHexagonPoint(OnHexagonPoint aOnPoint) {
            foreach (KeyValuePair<(int, int, int), HexCornerPoint> pt in cornerPoints) {
                aOnPoint(pt.Value);
            }
        }

        /// <summary>
        /// Execute on each hexagon line
        /// </summary>
        /// <param name="aOnLine">Code that will execute on each cell line</param>
        public void onHexagonLine(OnHexagonLine aOnLine) {

            List<(int, int, int)> visitedPoints = new List<(int, int, int)>();

            //Loop through each hex corner point
            foreach(KeyValuePair<(int, int, int), HexCornerPoint> cornerPt in cornerPoints) {

                //Loop through the adjacent points
                foreach((int,int,int) pt in cornerPt.Value.connectingPoints) {

                    //If we have visited this point before then ignore it
                    if(visitedPoints.Contains(pt)) {
                        continue;
                    }

                    //Execute on the line
                    aOnLine(cornerPoints[cornerPt.Key], cornerPoints[pt]);

                }

                //Add the current point to the visited points
                visitedPoints.Add(cornerPt.Key);
            }

        }

        /// <summary>
        /// Execute on each hexagon
        /// </summary>
        /// <param name="aOnCell">The logic we will execute on each cell</param>
        public void onHexagon(OnHexagon aOnCell) {
            foreach (KeyValuePair<int, T> cell in cells) {
                aOnCell(cell.Value);
            }
        }

        /// <summary>
        /// Get the neighbours of a hexagon
        /// </summary>
        /// <param name="aHexagon">The selected hexagon</param>
        /// <returns>All of the corresponding neighbours</returns>
        public List<T> getNeighbours(T aHexagon) {
            List<T> neighbours = new List<T>();

            foreach(int index in aHexagon.nearbyCells) {
                neighbours.Add(cells[index]);
            }

            return neighbours;
        }

        /// <summary>
        /// Get the neighbours of a hexagon
        /// </summary>
        /// <param name="aColoum">The coloum of the hexagon</param>
        /// <param name="aRow">The row of the hexagon</param>
        /// <param name="aIndex">The index that identifies the hexagon</param>
        /// <returns>Returns a list of nearby neighbours</returns>
        private List<int> _getHexagonNeighbours(int aColoum, int aRow, int aIndex) {
            bool evenRow = aRow % 2 == 0;

            //Get the neighbours of the cell
            List<int> nearbyNeighbours = new List<int>();

            //Neighbours of even row hexagons
            if (evenRow) {

                if (aColoum > 0 && aRow < height - 1) {
                    nearbyNeighbours.Add(aIndex + width - 1); // Top Left
                }
                if (aRow < height - 1) {
                    nearbyNeighbours.Add(aIndex + width); // Top Right
                }
                if (aColoum < width - 1) {
                    nearbyNeighbours.Add(aIndex + 1); // Right
                }
                if (aRow > 0) {
                    nearbyNeighbours.Add(aIndex - width); // Bottom Right
                }
                if (aColoum > 0 && aRow > 0) {
                    nearbyNeighbours.Add(aIndex - width - 1); // Bottom Left
                }
                if (aColoum > 0) {
                    nearbyNeighbours.Add(aIndex - 1); // Left
                }
            
            //Neighbours of odd row hexagons
            } else {

                if (aRow < height - 1) {
                    nearbyNeighbours.Add(aIndex + width); // Top Left
                }
                if (aRow < height - 1 && aColoum < width - 1) {
                    nearbyNeighbours.Add(aIndex + width + 1); // Top Right
                }
                if (aColoum < width - 1) {
                    nearbyNeighbours.Add(aIndex + 1); // Right
                }
                if (aRow > 0 && aColoum < width - 1) {
                    nearbyNeighbours.Add(aIndex - width + 1); // Bottom Right
                }
                if (aRow > 0) {
                    nearbyNeighbours.Add(aIndex - width); // Bottom Left
                }
                if (aColoum > 0) {
                    nearbyNeighbours.Add(aIndex - 1); // Left
                }

            }

            return nearbyNeighbours;
        }

        /// <summary>
        /// Construct a new hexagon
        /// </summary>
        /// <param name="aIndex">The index that identifies the hexagon</param>
        /// <param name="aColoum">The coloum of the hexagon</param>
        /// <param name="aRow">The row of the hexagon</param>
        private void _constuctHexagon(int aIndex, int aColoum, int aRow) {
            
            //Create a new instance of the cell
            T newCell = Activator.CreateInstance<T>();

            //Setup the details of the cell
            newCell.index       = aIndex;
            newCell.nearbyCells = _getHexagonNeighbours(aColoum, aRow, aIndex);
            newCell.coloum      = aColoum;
            newCell.row         = aRow;
            newCell.gridWidth   = width;
            newCell.gridHeight  = height;

            //Add the cell to the list
            cells.Add(aIndex, newCell);
        }

        /// <summary>
        /// Construct the top right corner connections for hexagons on the even rows
        /// </summary>
        /// <param name="aIndex">The index of the hexagon</param>
        /// <param name="aColoum">The coloum of the hexagon</param>
        /// <param name="aRow">The row of the hexagon</param>
        /// <returns>Returns the top right corner point of a hexagon</returns>
        private HexCornerPoint _evenTopRightCornerConnections(int aIndex, int aColoum, int aRow) {
            
            //Construct the corner point
            (int, int, int) tRCorner = (aIndex, aIndex + 1, aIndex + width);
            HexCornerPoint topRightCornerPt = new HexCornerPoint(tRCorner);

            //Connect the point to the adjacent points
            //Top Right
            topRightCornerPt.connectingPoints.Add((tRCorner.Item1 + 1, tRCorner.Item2 + width - 1, tRCorner.Item3 + 1));
            if (aColoum > 0) {
                //Top Left
                topRightCornerPt.connectingPoints.Add((tRCorner.Item1, tRCorner.Item2 + width - 2, tRCorner.Item3));
            }
            if (aRow > 0) {
                //Bottom
                topRightCornerPt.connectingPoints.Add((tRCorner.Item1 - width, tRCorner.Item2 - 1, tRCorner.Item3 - width + 1));
            }

            return topRightCornerPt;

        }

        /// <summary>
        /// Construct the top corner connections for hexagons on the even rows
        /// </summary>
        /// <param name="aIndex">The index of the hexagon</param>
        /// <param name="aColoum">The coloum of the hexagon</param>
        /// <param name="aRow">The row of the hexagon</param>
        /// <returns>Returns the top corner point of a hexagon</returns>
        private HexCornerPoint _evenTopCornerConnections(int aIndex, int aColoum, int aRow) {

            (int, int, int) tCorner = (aIndex, aIndex + width - 1, aIndex + width);
            HexCornerPoint topCornerPt = new HexCornerPoint(tCorner);

            //Bottom left
            topCornerPt.connectingPoints.Add((tCorner.Item1 - 1, tCorner.Item2 - width + 1, tCorner.Item3 - 1));

            if (aColoum < width - 1) {
                //Bottom right
                topCornerPt.connectingPoints.Add((tCorner.Item1, tCorner.Item2 - width + 2, tCorner.Item3));
            }

            //Top
            if (aRow < height - 2) {
                topCornerPt.connectingPoints.Add((tCorner.Item1 + width - 1, tCorner.Item2 + 1, tCorner.Item3 + width));
            }

            return topCornerPt;
        }

        /// <summary>
        /// Construct the top corner connections for hexagons on the odd rows
        /// </summary>
        /// <param name="aIndex">The index of the hexagon</param>
        /// <param name="aColoum">The coloum of the hexagon</param>
        /// <param name="aRow">The row of the hexagon</param>
        /// <returns>Returns the top corner point of a hexagon</returns>
        private HexCornerPoint _oddTopCornerConnections(int aIndex, int aColoum, int aRow) {
            (int, int, int) tCorner = (aIndex, aIndex + width, aIndex + width + 1);
            HexCornerPoint topCornerPt = new HexCornerPoint(tCorner);

            if (aColoum > 0) {
                //Bottom left
                topCornerPt.connectingPoints.Add((tCorner.Item1 - 1, tCorner.Item2 - width, tCorner.Item3 - 1));
            }

            if (aColoum < width - 1) {
                //Bottom right
                topCornerPt.connectingPoints.Add((tCorner.Item1, tCorner.Item2 - width + 1, tCorner.Item3));
            }

            //Top
            if (aRow < height - 2) {
                topCornerPt.connectingPoints.Add((tCorner.Item1 + width, tCorner.Item2 + 1, tCorner.Item3 + width - 1));
            }

            return topCornerPt;
        }

        /// <summary>
        /// Construct the top right corner connections for hexagons on the odd rows
        /// </summary>
        /// <param name="aIndex">The index of the hexagon</param>
        /// <param name="aColoum">The coloum of the hexagon</param>
        /// <param name="aRow">The row of the hexagon</param>
        /// <returns>Returns the top right corner point of a hexagon</returns>
        private HexCornerPoint _oddTopRightCornerConnections(int aIndex, int aColoum, int aRow) {

            (int, int, int) tRCorner = (aIndex, aIndex + 1, aIndex + width + 1);
            HexCornerPoint topRightCornerPt = new HexCornerPoint(tRCorner);

            if (aColoum < width - 2) {
                //Top Right
                topRightCornerPt.connectingPoints.Add((tRCorner.Item1 + 1, tRCorner.Item2 + width, tRCorner.Item3 + 1));
            }

            //Top Left
            topRightCornerPt.connectingPoints.Add((tRCorner.Item1, tRCorner.Item2 + width - 1, tRCorner.Item3));

            if (aRow > 0) {
                //Bottom
                topRightCornerPt.connectingPoints.Add((tRCorner.Item1 - width + 1, tRCorner.Item2 - 1, tRCorner.Item3 - width));
            }

            return topRightCornerPt;

        }

        /// <summary>
        /// Construct the corner data points for the hexagons
        /// </summary>
        /// <param name="aIndex">The index of the hexagon</param>
        /// <param name="aColoum">The coloum of the hexagon</param>
        /// <param name="aRow">The row of the hexagon</param>
        private void _constructCornerDataPoints(int aIndex, int aColoum, int aRow) {

            //Get the cell that corresponds with the index
            T cell = cells[aIndex];
            
            //For all of the even rows
            if (aRow % 2 == 0) {
                if (aRow < height - 1) {
                    if (aColoum < width - 1) {
                        //Get the top right corner point of the cell
                        HexCornerPoint topRightCornerPt = _evenTopRightCornerConnections(aIndex, aColoum, aRow);
                        cornerPoints.Add(topRightCornerPt.index, topRightCornerPt);
                    }
                    if (aColoum > 0) {
                        //Get the top corner point of the cell
                        HexCornerPoint topCornerPt = _evenTopCornerConnections(aIndex, aColoum, aRow);
                        cornerPoints.Add(topCornerPt.index, topCornerPt);
                    }
                }
            //For all of the odd rows
            } else {
                if (aRow < height - 1) {
                    if (aColoum < width - 1) {
                        //Get the top right corner point of the cell
                        HexCornerPoint topCornerPt = _oddTopCornerConnections(aIndex, aColoum, aRow);
                        cornerPoints.Add(topCornerPt.index, topCornerPt);
                    }
                    if (aColoum < width - 1) {
                        //Get the top corner point of the cell
                        HexCornerPoint topRightCornerPt = _oddTopRightCornerConnections(aIndex, aColoum, aRow);
                        cornerPoints.Add(topRightCornerPt.index, topRightCornerPt);
                    }
                }
            }
        }

        /// <summary>
        /// Construct the hexagons in the grid
        /// </summary>
        private void _constructHexagons() {
            for (int z = 0, i = 0; z < height; z++) {
                for (int x = 0; x < width; x++) {
                    //Construct the hexagon
                    _constuctHexagon(i, x, z);

                    //Construct the corner data points
                    _constructCornerDataPoints(i, x, z);
                    
                    i++;
                }
            }
        }
        
        #endregion

        #region Constructors

        /// <summary>
        /// Construct a Hexagon Grid
        /// </summary>
        /// <param name="aWidth">The width of the grid</param>
        /// <param name="aHeight">The height of the grid</param>
        public HexGrid(int aWidth, int aHeight) {
            width           = aWidth;
            height          = aHeight;
            cells           = new Dictionary<int, T>();
            cornerPoints    = new Dictionary<(int, int, int), HexCornerPoint>();

            //Construct the grids
            _constructHexagons();
        }

        #endregion

    }

    /// <summary>
    /// A corner point of a hexagon
    /// </summary>
    public class HexCornerPoint {

        #region Variables

        /// <summary>
        /// The index of the corner point
        /// </summary>
        public (int, int, int) index { get; private set; }

        /// <summary>
        /// The tags assigned to the corner points
        /// </summary>
        public List<string> tags;

        /// <summary>
        /// A list of all the adjacent hexagon points
        /// </summary>
        public List<(int, int, int)> connectingPoints;

        #endregion

        #region Constructors

        /// <summary>
        /// Construct a hex corner point
        /// </summary>
        /// <param name="aCornerPt">The three hexagons that are connected to this point</param>
        public HexCornerPoint((int, int, int) aCornerPt) {
            index = aCornerPt;
            tags = new List<string>();
            connectingPoints = new List<(int, int, int)>();
        }

        #endregion

    }

}