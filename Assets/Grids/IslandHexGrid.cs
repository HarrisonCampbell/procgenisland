﻿using UnityEngine;
using Soup.Grids;
using Soup.Tools;
using System.Collections.Generic;


/// <summary>
/// Constructs a hexagon grid that resembles an island
/// </summary>
public class IslandHexGrid : HexGrid<IslandHexCell> {

    #region Variables

    /// <summary>
    /// The default complexity used for an island
    /// </summary>
    public const float DEFAULT_ISLAND_COMPLEXITY = 3f;

    /// <summary>
    /// The complexity of the island, the higher the value the more complex the island
    /// </summary>
    public float islandComplexity { get; private set; }

    /// <summary>
    /// The outer radius of the hexagon
    /// </summary>
    public float outerRadius { get; private set; }

    /// <summary>
    /// The inner radius of the hexagon
    /// </summary>
    public float innerRadius {
        get {
            return outerRadius * 0.866025404f;
        }
    }
    
    #endregion

    #region Functions

    /// <summary>
    /// Get the world type based on the noise
    /// </summary>
    /// <param name="aNoise">The noise ranging from 0 to 1</param>
    /// <returns>Returns the corresponding world type based on the noise</returns>
    private IslandHexCell.WorldType _getWorldType(float aNoise) {

        IslandHexCell.WorldType type = IslandHexCell.WorldType.NONE;

        if (aNoise <= 0.2f) {
            type = IslandHexCell.WorldType.WATER;
        } else if (aNoise <= 0.3f) {
            type = IslandHexCell.WorldType.UNDER_WATER;
        } else if (aNoise <= 0.4f) {
            type = IslandHexCell.WorldType.SAND;
        } else if (aNoise <= 0.8f) {
            type = IslandHexCell.WorldType.GRASS;
        } else {
            type = IslandHexCell.WorldType.MOUNTAIN;
        }

        return type;
    }

    /// <summary>
    /// Get the density type based on the noise
    /// </summary>
    /// <param name="aNoise">The noise ranging from 0 to 1</param>
    /// <returns>Returns the density type</returns>
    private IslandHexCell.DensityType _getDensityType(float aNoise) {

        IslandHexCell.DensityType type = IslandHexCell.DensityType.NONE;

        if (aNoise <= 0.1f) {
            type = IslandHexCell.DensityType.NONE;
        } else if(aNoise <= 0.4f) {
            type = IslandHexCell.DensityType.LIGHT;
        } else if(aNoise <= 0.6f) {
            type = IslandHexCell.DensityType.MEDIUM;
        } else {
            type = IslandHexCell.DensityType.DENSE;
        }

        return type;
    }

    /// <summary>
    /// Get the world data for a given cell
    /// </summary>
    /// <param name="aCell">The cell we want to get the extra data for</param>
    private void _getWorldData(IslandHexCell aCell) {

        //Randomise the density of the island
        float densityComplexity = Random.value * 5f;

        //Get the world type of the cell
        float xPos = (float)aCell.coloum / (float)width;
        float yPos = (float)aCell.row / (float)height;
        float worldNoise    = NoiseFunctions.islandNoiseNormalised(xPos, yPos, islandComplexity);
        worldNoise          = (float)System.Math.Round(worldNoise, 1);
        aCell.worldNoiseHeight = worldNoise;
        aCell.worldType     = _getWorldType(worldNoise);

        //Get the density of the cell
        float densityNoise  = NoiseFunctions.islandNoiseNormalised(xPos, yPos, densityComplexity);
        aCell.densityType   = _getDensityType(densityNoise);

        //Get the position of the cell
        Vector3 newPosition = new Vector3(
            (aCell.coloum + aCell.row * 0.5f - aCell.row / 2) * (innerRadius * 2f),
            worldNoise * 11,
            aCell.row * (outerRadius * 1.5f)
        );
        aCell.position = newPosition;

    }

    /// <summary>
    /// Add corner data to each hexagon
    /// </summary>
    /// <param name="aPoint">The point we are executing the code on</param>
    private void _addCornerData(HexCornerPoint aPoint) {

        //Randomly add the wall tag to each corner point
        if(Random.value < 0.25f) {
            aPoint.tags.Add("Walls");
        }
    }

    #endregion
    
    #region Constructors

    /// <summary>
    /// Construct the hexagon grid for the island
    /// </summary>
    /// <param name="aRadius">The outer radius of the hexagon</param>
    /// <param name="aWidth">The number of coloums in the grid</param>
    /// <param name="aHeight">The number of rows in the grid</param>
    /// <param name="aComplexity">The complexity of the island</param>
    public IslandHexGrid(float aRadius, int aWidth, int aHeight, float aComplexity = DEFAULT_ISLAND_COMPLEXITY) 
        : base(aWidth, aHeight) {

        //Set the outer raidus of the hexagons, and the complexity of the island
        outerRadius = aRadius;

        islandComplexity = aComplexity;

        //Get the world data of each hexagon and point
        onHexagon(_getWorldData);
        onHexagonPoint(_addCornerData);

    }

    #endregion

}

/// <summary>
/// Hex cell that contains the island
/// </summary>
public class IslandHexCell : HexCell {

    #region Variables

    /// <summary>
    /// The type of the cell
    /// </summary>
    public enum WorldType {
        NONE = -1,
        WATER = 0, 
        UNDER_WATER = 1,
        SAND = 2,
        GRASS = 3,
        MOUNTAIN = 4
    }

    /// <summary>
    /// The density of the cell
    /// </summary>
    public enum DensityType {
        NONE = 0,
        LIGHT = 1,
        MEDIUM = 2,
        DENSE = 3
    }

    /// <summary>
    /// The world type of the cell
    /// </summary>
    public WorldType worldType;

    /// <summary>
    /// The density type of the cell
    /// </summary>
    public DensityType densityType;

    /// <summary>
    /// The position of the cell
    /// </summary>
    public Vector3 position;

    /// <summary>
    /// The height of the noise of the world
    /// </summary>
    public float worldNoiseHeight;

    #endregion

}
