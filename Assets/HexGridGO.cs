﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Soup.Tools;
using Soup.Grids;
using Soup.Tools.Primitives;

/// <summary>
/// Create the island in the unity scene
/// </summary>
/// <remarks>
/// This class is just for testing
/// </remarks>
public class HexGridGO : MonoBehaviour {

    #region Variables

    /// <summary>
    /// The width of the grid
    /// </summary>
    public int width = 6;

    /// <summary>
    /// The height of the grid
    /// </summary>
    public int height = 6;

    /// <summary>
    /// The outer radius of the hexagon in meters
    /// </summary>
    public float hexagonRadius = 5f;
    
    /// <summary>
    /// The grid of the island
    /// </summary>
    public IslandHexGrid grid;

    /// <summary>
    /// Asset selector
    /// </summary>
    public AssetSelector selector;

    /// <summary>
    /// The material used for the hexagon tiles
    /// </summary>
    public Material hexagonMaterial;

    /// <summary>
    /// The gradient for the beaches
    /// </summary>
    public Gradient beachGradient;

    /// <summary>
    /// The gradient for the grasses
    /// </summary>
    public Gradient grassGradient;

    /// <summary>
    /// The gradient for the mountains
    /// </summary>
    public Gradient mountainGradient;

    /// <summary>
    /// All of the objects that make up the island
    /// </summary>
    private Queue<GameObject> _islandObjects;

    #endregion

    #region Functions

    /// <summary>
    /// Get the world position of a HexCornerPoint
    /// </summary>
    /// <param name="aCornerPt">The HexCornerPoint</param>
    /// <returns>Returns the world position</returns>
    private Vector3 _getCornerPosition(HexCornerPoint aCornerPt) {
        // Get the average point between the cells, and the maximum height
        Vector3 averagePoint = Vector3.zero;
        float maxHeight = Mathf.Max(
            grid.cells[aCornerPt.index.Item1].position.y,
            grid.cells[aCornerPt.index.Item2].position.y,
            grid.cells[aCornerPt.index.Item3].position.y
        );
        averagePoint += grid.cells[aCornerPt.index.Item1].position;
        averagePoint += grid.cells[aCornerPt.index.Item2].position;
        averagePoint += grid.cells[aCornerPt.index.Item3].position;
        averagePoint /= 3;
        averagePoint.Set(averagePoint.x, maxHeight, averagePoint.z);
        return averagePoint;
    }

    /// <summary>
    /// Create walls along the edges of the hexagons
    /// </summary>
    /// <param name="aPt1">The first hexagon point</param>
    /// <param name="aPt2">The second hexagon point</param>
    private void _createWalls(HexCornerPoint aPt1, HexCornerPoint aPt2) {

        float pillarHeight = 1.5f;
        float wallHeight = 1f;

        int point1Max = 0;
        int point2Max = 0;

        // If there is a wall tag assigned to the first point, then create a wall pillar
        if (aPt1.tags.Contains("Walls")) {

            IslandHexCell cell1 = grid.cells[aPt1.index.Item1];
            IslandHexCell cell2 = grid.cells[aPt1.index.Item2];
            IslandHexCell cell3 = grid.cells[aPt1.index.Item3];
            point1Max = System.Math.Max((int)cell1.worldType, System.Math.Max((int)cell2.worldType, (int)cell3.worldType));

            if (point1Max >= (int)IslandHexCell.WorldType.SAND) {
                GameObject pillar = _createWallPillar(aPt1, pillarHeight);
                _islandObjects.Enqueue(pillar);
            }

        }

        // If there is a wall tag assigned to the second point, then create a wall pillar
        if (aPt2.tags.Contains("Walls")) {

            IslandHexCell cell1 = grid.cells[aPt2.index.Item1];
            IslandHexCell cell2 = grid.cells[aPt2.index.Item2];
            IslandHexCell cell3 = grid.cells[aPt2.index.Item3];
            point2Max = System.Math.Max((int)cell1.worldType, System.Math.Max((int)cell2.worldType, (int)cell3.worldType));

            if (point2Max >= (int)IslandHexCell.WorldType.SAND) {
                GameObject pillar = _createWallPillar(aPt2, pillarHeight);
                _islandObjects.Enqueue(pillar);
            }
        }

        // If both pillars have been created, and the pillars are not in the water then create a wall between them
        if (point1Max >= (int)IslandHexCell.WorldType.SAND && point2Max >= (int)IslandHexCell.WorldType.SAND) {
            if (aPt1.tags.Contains("Walls") && aPt2.tags.Contains("Walls")) {
                Vector3 worldPt1 = _getCornerPosition(aPt1);
                Vector3 worldPt2 = _getCornerPosition(aPt2);

                float minHeight = Mathf.Min(worldPt1.y, worldPt2.y);

                Mesh wallMesh = PrimitiveMeshShapes.createClosedWall(
                    new List<Vector3>() {
                    new Vector3(worldPt1.x, minHeight, worldPt1.z),
                    new Vector3(worldPt2.x, minHeight, worldPt2.z)
                    },
                    wallHeight
                );
                GameObject wallGO = new GameObject();
                wallGO.AddComponent<MeshFilter>().mesh              = wallMesh;
                wallGO.AddComponent<MeshRenderer>().material        = hexagonMaterial;
                wallGO.GetComponent<MeshRenderer>().material.color  = Color.red;
                _islandObjects.Enqueue(wallGO);
            }
        }
        
    }

    /// <summary>
    /// Create a wall pillar
    /// </summary>
    /// <param name="aPt">The hex corner point we want to have the pillar</param>
    /// <param name="aPillarHeight">The height of the pillar</param>
    /// <returns>Returns the gameobject of the pillar</returns>
    private GameObject _createWallPillar(HexCornerPoint aPt, float aPillarHeight) {

        // Get the average point between the cells
        Vector3 worldPoint1 = _getCornerPosition(aPt);
        Mesh hexMesh = _createHexMesh(1f, worldPoint1.y + aPillarHeight);

        GameObject hexGO = new GameObject();
        hexGO.AddComponent<MeshFilter>().mesh = hexMesh;
        hexGO.AddComponent<MeshRenderer>().material = hexagonMaterial;
        hexGO.GetComponent<MeshRenderer>().material.color = Color.red;
        hexGO.transform.position = worldPoint1 + Vector3.up * aPillarHeight;

        return hexGO;
    }

    /// <summary>
    /// Create the mesh of a hexagon object
    /// </summary>
    /// <param name="aRadius">The outer radius of the hexagon</param>
    /// <param name="aHeight">The height of the hexagon</param>
    /// <returns>Returns the mesh of the hexagon object</returns>
    private Mesh _createHexMesh(float aRadius, float aHeight) {
        List<Vector3> hexagonOutline = PrimitiveShapes.hexagonOutline(aRadius);
        hexagonOutline = hexagonOutline.ConvertAll((aPt) => new Vector3(aPt.x, -aHeight, aPt.z));

        Mesh topMesh = PrimitiveMeshShapes.createHexagon(aRadius, true);
        Mesh sideMesh = PrimitiveMeshShapes.createClosedWall(hexagonOutline, aHeight);
        MeshTools.convertToFlatMesh(ref sideMesh);

        CombineInstance[] instances = new CombineInstance[] {
            new CombineInstance() {
                mesh        = topMesh,
                transform   = Matrix4x4.identity
            },
            new CombineInstance() {
                mesh        = sideMesh,
                transform   = Matrix4x4.identity
            }
        };

        Mesh combinedMesh = new Mesh();
        combinedMesh.CombineMeshes(instances, true);

        return combinedMesh;
    }

    /// <summary>
    /// Draw the base of the hexagon cell
    /// </summary>
    /// <param name="aCell">The hexagon cell</param>
    /// <returns>Returns the gameobject of the hexagon cell</returns>
    private GameObject _drawBaseOfCell(IslandHexCell aCell) {

        Mesh combinedMesh = _createHexMesh(grid.outerRadius, aCell.worldNoiseHeight * 11);

        GameObject hexGO = new GameObject();
        hexGO.AddComponent<MeshFilter>().mesh = combinedMesh;
        hexGO.AddComponent<MeshRenderer>().material = hexagonMaterial;

        switch (aCell.worldType) {
            case IslandHexCell.WorldType.UNDER_WATER:
            case IslandHexCell.WorldType.SAND:
                hexGO.GetComponent<MeshRenderer>().material.color = beachGradient.Evaluate(Random.value);
                break;
            case IslandHexCell.WorldType.GRASS:
                hexGO.GetComponent<MeshRenderer>().material.color = grassGradient.Evaluate(Random.value);
                break;
            case IslandHexCell.WorldType.MOUNTAIN:
                hexGO.GetComponent<MeshRenderer>().material.color = mountainGradient.Evaluate(Random.value);
                break;
        }

        hexGO.transform.position = aCell.position;
        hexGO.name = "Index : " + aCell.index;
        return hexGO;
    }

    /// <summary>
    /// Add item to the top of the hexagon cell
    /// </summary>
    /// <param name="aCell">The hexagon cell</param>
    /// <returns>Returns the item on top of the hexagon cell</returns>
    private GameObject _addItemOnTop(IslandHexCell aCell) {

        if (aCell.densityType == IslandHexCell.DensityType.NONE || aCell.worldType != IslandHexCell.WorldType.GRASS) {
            return null;
        }

        float x = (float)(aCell.coloum) / (float)width;
        float y = (float)(aCell.row) / (float)height;
        float cityNoise = NoiseFunctions.islandNoiseNormalised(x, y, 4);

        string group = aCell.worldType.ToString().ToLower() + "-" + aCell.densityType.ToString().ToLower();

        GameObject itemGO = Instantiate(selector.getAsset(group));
        itemGO.transform.position = aCell.position;
        return itemGO;
    }

    /// <summary>
    /// Create the cell
    /// </summary>
    /// <param name="aCell">The hexagon cell</param>
    private void _createCell(IslandHexCell aCell) {

        // Only create cells that are on the island
        if (aCell.worldType < IslandHexCell.WorldType.SAND) {
            return;
        }

        //Create the cell and add the item on top
        GameObject hexGO    = _drawBaseOfCell(aCell);
        _islandObjects.Enqueue(hexGO);
        GameObject hexItem  = _addItemOnTop(aCell);

        // If the item has been created for the cell, then make the item face a lower cell
        if (hexItem != null) {
            _islandObjects.Enqueue(hexItem);

            bool foundHex = false;
            List<IslandHexCell> neighbours = grid.getNeighbours(aCell);
            List<IslandHexCell> sameLevel = new List<IslandHexCell>();

            foreach (IslandHexCell cell in neighbours) {
                if (cell.worldNoiseHeight < aCell.worldNoiseHeight) {
                    foundHex = true;
                    hexItem.transform.LookAt(new Vector3(cell.position.x, aCell.position.y, cell.position.z));
                    break;
                } else if (cell.worldNoiseHeight == aCell.worldNoiseHeight) {
                    sameLevel.Add(cell);
                }
            }

            if (!foundHex) {
                if(sameLevel.Count > 0) {
                    hexItem.transform.LookAt(new Vector3(sameLevel[0].position.x, aCell.position.y, sameLevel[0].position.z));
                } else {
                    //If there are no cells below or on the same level as the current cell, then select the first cell to look at
                    hexItem.transform.LookAt(new Vector3(neighbours[0].position.x, aCell.position.y, neighbours[0].position.z));
                }
            }

        }
        
    }
    
    /// <summary>
    /// Reset the island
    /// </summary>
    public void resetIsland() {

        //Destroy all of the island objects before creating it again
        while(_islandObjects.Count > 0) {
            GameObject obj = _islandObjects.Dequeue();
            Destroy(obj);
        }
        
        //Each time we create the island randomly set the complexity
        float islandComplexity = Random.value * 5f;

        grid = new IslandHexGrid(hexagonRadius, width, height, islandComplexity);
        grid.onHexagon(_createCell);
        grid.onHexagonLine(_createWalls);

    }

    #endregion

    #region Unity Functions

    void Start() {
        _islandObjects  = new Queue<GameObject>();
        selector        = new AssetSelector("IslandAssets");

        resetIsland();
    }

    #endregion



}
