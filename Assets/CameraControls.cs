﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControls : MonoBehaviour {

    #region Variables

    /// <summary>
    /// The minimum orthographic size allowed for the camera
    /// </summary>
    private const float _MIN_ORTH_SIZE = 10f;

    /// <summary>
    /// The maximum orthographic size allowed for the camera
    /// </summary>
    private const float _MAX_ORTH_SIZE = 80f;

    /// <summary>
    /// The speed that the camera pans at
    /// </summary>
    public float cameraPanSpeed = 5f;

    /// <summary>
    /// The speed that the camera zooms at
    /// </summary>
    public float cameraZoomSpeed = 1f;

    /// <summary>
    /// The camera object that we are affecting
    /// </summary>
    public Camera cam { get; private set; }

    #endregion

    #region Functions

    public void panCamera(Vector2 aDir) {
        //Rotate the direction to face the front
        Vector3 vec = new Vector3(aDir.x, 0, aDir.y);
        vec = Quaternion.Euler(0, transform.localEulerAngles.y + 180f, 0) * vec;
        transform.position += vec * Time.deltaTime * cameraPanSpeed;
    }

    public void zoomCamera(float aZoom) {
        if(cam.orthographic) {
            float newSize = Mathf.Clamp(cam.orthographicSize - (aZoom * cameraZoomSpeed), _MIN_ORTH_SIZE, _MAX_ORTH_SIZE);
            cam.orthographicSize = newSize;
        } else {
            transform.position += transform.forward * aZoom * cameraZoomSpeed;
        }
    }

    public void rotateCamera(float aRot) {
        transform.localEulerAngles += new Vector3(0, aRot, 0);
    }

    #endregion

    private void Start() {
        cam = GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update() {
        
        if(Input.GetMouseButton(0)) {
            panCamera(new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y")));
        }

        zoomCamera(Input.mouseScrollDelta.y);

        if(Input.GetMouseButton(1)) {
            rotateCamera(Input.GetAxis("Mouse X"));
        }

    }
}
