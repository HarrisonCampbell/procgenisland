﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Soup.Tools {

    /// <summary>
    /// Used to select the correct asset
    /// </summary>
    public class AssetSelector {

        #region Variables

        /// <summary>
        /// Path to the group of assets
        /// </summary>
        public string path { get; private set; }

        /// <summary>
        /// All of the asset groups loaded
        /// </summary>
        public Dictionary<string, GameObject[]> assetGroups;

        #endregion

        #region Functions

        /// <summary>
        /// Get all of the assets of a given group
        /// </summary>
        /// <param name="aGroup">The selected group</param>
        /// <returns>Returns all of the assets of the group</returns>
        public GameObject[] getAssets(string aGroup) {

            if (!assetGroups.ContainsKey(aGroup)) {
                GameObject[] assets = Resources.LoadAll<GameObject>(path + "/" + aGroup);
                assetGroups.Add(aGroup, assets);
            }

            return assetGroups[aGroup];
        }

        /// <summary>
        /// Get a random asset from a group
        /// </summary>
        /// <param name="aGroup">The selected group</param>
        /// <returns>Returns a random asset from a group</returns>
        public GameObject getAsset(string aGroup) {
            GameObject[] assets = getAssets(aGroup);
            if(assets.Length > 0) {
                return assets[(int)(assets.Length * Random.value)];
            } else {
                return null;
            }
        }

        #endregion

        #region Constructors

        /// <summary>
        /// Construct an instance of the asset selector
        /// </summary>
        /// <param name="aPath">The path of the assets</param>
        public AssetSelector(string aPath) {
            path        = aPath;
            assetGroups = new Dictionary<string, GameObject[]>();
        }

        #endregion

    }

}
