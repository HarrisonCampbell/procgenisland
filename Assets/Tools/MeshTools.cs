﻿using UnityEngine;
using System.Collections;

namespace Soup.Tools {

    /// <summary>
    /// All tools that affect meshes
    /// </summary>
    public static class MeshTools {

        #region Functions

        /// <summary>
        /// Convert a mesh to a flat mesh
        /// </summary>
        /// <param name="aMesh">The mesh we want to convert, assumes it's not already a flat mesh</param>
        public static void convertToFlatMesh(ref Mesh aMesh) {

            Vector3[] oldVerts = aMesh.vertices;
            int[] triangles = aMesh.triangles;
            Vector3[] vertices = new Vector3[triangles.Length];
            for (int i = 0; i < triangles.Length; i++) {
                vertices[i] = oldVerts[triangles[i]];
                triangles[i] = i;
            }

            aMesh.vertices      = vertices;
            aMesh.triangles     = triangles;
            aMesh.RecalculateBounds();
            aMesh.RecalculateNormals();
            aMesh.RecalculateTangents();
        }

        #endregion
    }
    
}

