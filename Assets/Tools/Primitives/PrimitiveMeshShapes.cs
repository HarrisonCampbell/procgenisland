﻿using UnityEngine;
using System;
using System.Collections.Generic;

namespace Soup.Tools.Primitives {

    /// <summary>
    /// Holds functions that can create simple primitive mesh shapes
    /// </summary>
    public static class PrimitiveMeshShapes {

        #region Functions

        /// <summary>
        /// Get the line mesh data of an outline
        /// </summary>
        /// <param name="aOutline">The outline</param>
        /// <param name="aHeight">The height of the line</param>
        /// <returns>Returns the vertices and triangles of the mesh</returns>
        private static (List<Vector3>, List<int>) _getLineMeshData(List<Vector3> aOutline, float aHeight) {
            //Get the top line of the wall
            List<Vector3> topLine = new List<Vector3>();
            for (int i = 0; i < aOutline.Count; i++) {
                topLine.Add(aOutline[i] + Vector3.up * aHeight);
            }

            List<Vector3> vertices = new List<Vector3>();
            List<int> triangles = new List<int>();

            //At the beginning, add the starting points
            vertices.Add(aOutline[0]);
            vertices.Add(topLine[0]);

            //Construct the mesh for the wall
            int triIndex = 0;
            for (int i = 0; i < aOutline.Count - 1; i++) {
                Vector3 currBtPt = aOutline[i];
                Vector3 currTpPt = topLine[i];
                Vector3 nextBtPt = aOutline[i + 1];
                Vector3 nextTpPt = topLine[i + 1];

                vertices.Add(nextBtPt);
                vertices.Add(nextTpPt);

                triangles.Add(triIndex);
                triangles.Add(triIndex + 1);
                triangles.Add(triIndex + 2);

                triangles.Add(triIndex + 1);
                triangles.Add(triIndex + 3);
                triangles.Add(triIndex + 2);

                triIndex += 2;
            }

            return (vertices, triangles);
        }

        /// <summary>
        /// Create the mesh for a wall
        /// </summary>
        /// <param name="aWallLine">The wall to follow for the wall</param>
        /// <param name="aHeight">The height of thwe wall</param>
        /// <returns>Returns the mesh for the wall</returns>
        public static Mesh createWall(List<Vector3> aWallLine, float aHeight) {

            (List<Vector3>, List<int>) meshData = _getLineMeshData(aWallLine, aHeight);

            Mesh mesh = new Mesh() {
                vertices    = meshData.Item1.ToArray(),
                triangles   = meshData.Item2.ToArray()
            };
            mesh.RecalculateBounds();
            mesh.RecalculateNormals();
            mesh.RecalculateTangents();

            return mesh;

        }
      
        /// <summary>
        /// Create the mesh for a closed wall, (closed hole)
        /// </summary>
        /// <param name="aOutline">The outline</param>
        /// <param name="aHeight">The height of the wall</param>
        /// <returns>Returns the mesh of the wall</returns>
        public static Mesh createClosedWall(List<Vector3> aOutline, float aHeight) {

            (List<Vector3>, List<int>) meshData = _getLineMeshData(aOutline, aHeight);

            //Connect the start and the end
            List<int> newTriangles = new List<int>();
            newTriangles.Add(0);
            newTriangles.Add(meshData.Item2[meshData.Item2.Count - 1]);
            newTriangles.Add(meshData.Item2[meshData.Item2.Count - 2]);

            newTriangles.Add(0);
            newTriangles.Add(meshData.Item2[meshData.Item2.Count - 2]);
            newTriangles.Add(1);

            meshData.Item2.AddRange(newTriangles);
            
            Mesh mesh = new Mesh();
            mesh.vertices = meshData.Item1.ToArray();
            mesh.triangles = meshData.Item2.ToArray();
            mesh.RecalculateBounds();
            mesh.RecalculateNormals();
            mesh.RecalculateTangents();

            return mesh;
        }
        
        /// <summary>
        /// Construct a hexagon mesh
        /// </summary>
        /// <param name="aRadius">The radius of the hexagon</param>
        /// <param name="aIsFacingUp">Is the outline facing up, or is it facing down</param>
        /// <returns>Returns a mesh of a hexagon</returns>
        public static Mesh createHexagon(float aRadius, bool aIsFacingUp = false) {
            Poly2Mesh.Polygon hexPoly = new Poly2Mesh.Polygon() {
                outside = PrimitiveShapes.hexagonOutline(aRadius)
            };
            if(aIsFacingUp) {
                hexPoly.outside.Reverse();
            }
            return Poly2Mesh.CreateMesh(hexPoly);
        }

        #endregion

    }

}
