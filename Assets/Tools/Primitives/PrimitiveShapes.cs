﻿using UnityEngine;
using System.Collections.Generic;

namespace Soup.Tools.Primitives {

    /// <summary>
    /// Holds functions that can create simple primitive shapes
    /// </summary>
    public static class PrimitiveShapes {

        #region Functions

        /// <summary>
        /// Get an outline of a shape, with all sides being equidesent 
        /// </summary>
        /// <param name="aRadius">The radius of the shape, all points of the shape should align on the radius</param>
        /// <param name="aSides">The number of sides of the shape</param>
        /// <param name="aRotOffset">How much we offset the rotation</param>
        /// <returns>Returns the outline of a shape, in a counter clockwise fashion</returns>
        public static List<Vector3> getShapeOutline(float aRadius, int aSides, float aRotOffset = 0) {

            Vector3 forward         = Vector3.forward * aRadius;
            float rotSegment   = 360f / aSides;

            List<Vector3> outline = new List<Vector3>();

            for(float i = 0; i < 360; i += rotSegment) {
                outline.Add(Quaternion.Euler(0, -i - aRotOffset, 0) * forward);
            }

            return outline;

        }

        /// <summary>
        /// Construct a hexagon outline
        /// </summary>
        /// <param name="aRadius">The exterior radius of a hexagon</param>
        /// <returns>Returns an outline of a hexagon, in a counter clockwise fashion</returns>
        public static List<Vector3> hexagonOutline(float aRadius, float aRotOffset = 0) {
            return getShapeOutline(aRadius, 6, aRotOffset);
        }

        #endregion

    }

}
