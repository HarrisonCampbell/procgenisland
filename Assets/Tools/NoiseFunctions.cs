using UnityEngine;

namespace Soup.Tools {

    /// <summary>
    /// Function that help generate noise maps
    /// </summary>
    public static class NoiseFunctions {
        
        /// <summary>
        /// Create perlin noise used to emulate an island
        /// </summary>
        /// <param name="aX">The x coordinate between 0 and the range</param>
        /// <param name="aY">The y coordinate between 0 and the range</param>
        /// <param name="aComplexity">The complexity of the island, the higher the number the more complex the island is</param>
        /// <returns>Returns a remapped perlin noise between 0 and 1</returns>
        public static float islandNoise(float aX, float aY, float aComplexity) {
            
            //Get the centre of the map, and the current point we are trying to find
            Vector2 centre  = new Vector2(aComplexity, aComplexity) * 0.5f;
            Vector2 point   = new Vector2(aX, aY);

            //Remap the point height
            float distToCentre  = Vector2.Distance(centre, point);
            float ratioToCentre = (distToCentre / aComplexity) * 2;
            float pointHeight   = Mathf.PerlinNoise(aX, aY);
            pointHeight         = (1 + pointHeight - ratioToCentre) * 0.5f;

            return pointHeight;
        }

        /// <summary>
        /// Create perlin noise used to emulate an island, assumes the x and y value are capped at 0 and 1
        /// </summary>
        /// <param name="aX">The x coordinate between 0 and 1</param>
        /// <param name="aY">The x coordinate between 0 and 1</param>
        /// <param name="aComplexity">The complexity of the island, the higher the number the more complex the island is</param>
        /// <returns>Returns a remapped perlin noise between 0 and 1</returns>
        public static float islandNoiseNormalised(float aX, float aY, float aComplexity) {
            return islandNoise(aX * aComplexity, aY * aComplexity, aComplexity);
        }

    }

}